<!DOCTYPE html>
<html lang="en">
<head>
<#include "parts/head.ftl">
    <title>Admin</title>
    <style>
        html {
            overflow-x: hidden
        }
    </style>
</head>
<body>
<container>

<#include "parts/header.ftl">

    <p class="zer"></p>

    <br>

    <br>
    <h1 style="text-align: left">ПАНЕЛЬ АДМИНИСТРАТОРА</h1>
    <br>

    <div class="row row-log">
        <div class="col-sm-2 lab">
            <h2><a>БД книг</a></h2>
        </div>
    </div>

    <div class="row row-log">
        <div class="col-sm-2 lab">
            <h2><a>БД авторов</a></h2>
        </div>
    </div>

    <div class="row row-log">
        <div class="col-sm-2 lab">
            <h2><a>БД рецензий</a></h2>
        </div>
    </div>

    <input type="submit" value="Выйти из режима администратора" class="btn btn-success btn-lg"
           style="position: relative; left: 45%; bottom: 5px;">

<#include "parts/footer.ftl">

    <script src="/templates/js/jquery.min.js"></script>
    <script src="/templates/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
</container>
</body>
</html>












