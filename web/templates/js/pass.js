function checkSymbols() {
    var pass = document.getElementById("pass");
    var re = /(?=^.{7,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
    if (re.test(pass.value)) {
        document.getElementById("atten").innerText = '';
    } else {
        document.getElementById("atten").innerText = 'Пароль должен содержать строчные и заглавные буквы латинского алфавита. Длина не менее 7 символов';
    }
}