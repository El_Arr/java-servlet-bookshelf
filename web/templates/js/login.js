function registrationSubmit() {
    var nameCheck = nameCheck();
    var usernameCheck = usernameCheck();
    var emailCheck = emailCheck();
    return usernameCheck && loginCheck && passwordCheck;
}

function nameCheck() {
    var name = $('#name').val();
    return name != null && name != '';
}

function usernameCheck() {
    var username = $('#username').val();
    return username != null && username != '';
}

function emailCheck() {
    var email = $('#email').val();
    return email != null && email != '';
}

function alert() {
    if (!registrationSubmit()) {
        $('#reg').on('submit', function(e) {
            e.preventDefault();
            console.log("Работает");
        });
        var header = document.getElementById('header');
        header.innerHTML = ['Заполните поля ввода'].join('');
    }
    return registrationSubmit();
}