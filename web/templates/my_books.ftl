<!DOCTYPE html>
<html lang="en">
<head>
<#include "parts/head.ftl">
    <title>My books</title>
</head>
<body>

<#include "parts/header.ftl">

<p class="zer"></p>

<h1>КНИЖНАЯ ПОЛКА</h1>

<section id="what-we-do">
    <div class="section-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                <#include "parts/book_author.ftl">
                </div>
            </div>
        </div>
    </div>
</section>

<#include "parts/footer.ftl">

<script src="/templates/js/jquery.min.js"></script>
<script src="/templates/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>












