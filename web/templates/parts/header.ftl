<nav id="nav" class="navbar navbar-dark bg-inverse navbar-full">
    <a class="navbar-brand" href="/"><img style="padding: 0; margin: 0" src="/templates/img/logo.jpg"></a>
    <ul class="nav navbar-nav" style="font-size: 23px; padding-top: 25px">
        <li class="nav-item">
            <a class="nav-l" href="/">Главная</a>
        </li>
        <li class="nav-item">
            <a class="nav-l" href="/mybooks">Книжная полка</a>
        </li>
        <li class="nav-item">
            <a class="nav-l" href="/books">Каталог</a>
        </li>
    <#if logged == true>
        <li class="nav-item">
            <a class="nav-l" href="/account">Личный кабинет</a>
        </li>
        <li class="nav-item">
            <a class="nav-l" href="/logout">Выйти</a>
        </li>
    <#else>
        <li class="nav-item">
            <a class="nav-l" href="/login">Войти</a>
        </li>
    </#if>
    </ul>
    <form class="form-inline pull-xs-right">
        <input class="form-control" type="text" name="q" id="q_id" oninput="f()" placeholder="Поиск">

        <ul id="results">
    </form>
</nav>

<script type="application/javascript">
    function f() {
        $.ajax({
            url: "/book-search",
            "data": {"q": $("#q_id").val()},
            "dataType": "json",
            success: function (result) {
                $("#results").html("");
                for (var i = 0; i < result.ids.length; i++) {
                    var href = "<a href='" + result.ids[i] + "'>";
                    $("#results").append("<li>" + href + result.names[i] + "</a></li>");
                }
                $("#results").append("</ul>")
            },
            error: function () {
                console.log("Ajax failed");
            }
        });
    }
</script>