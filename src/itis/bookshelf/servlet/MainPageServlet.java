package itis.bookshelf.servlet;

import itis.bookshelf.cfg.view.Freemarker;
import itis.bookshelf.service.entity.BookAuthorService;
import itis.bookshelf.service.entity.BookService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MainPageServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        Map<String, Object> root = new HashMap<>();
        if (request.getSession().getAttribute("current-user") != null)
            root.put("logged", true);
        else
            root.put("logged", false);
        root.put("books", BookService.getAll());
        root.put("books_authors", BookAuthorService.getAll());
        Freemarker.start("main_page.ftl", root, response);
    }
}
