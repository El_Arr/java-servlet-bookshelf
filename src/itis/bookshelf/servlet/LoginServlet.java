package itis.bookshelf.servlet;

import itis.bookshelf.cfg.view.Freemarker;
import itis.bookshelf.entity.User;
import itis.bookshelf.service.entity.UserService;
import itis.bookshelf.service.entity.UserTokenService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class LoginServlet extends HttpServlet {
    private Map<String, Object> root = new HashMap<>();

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        User current = (User) request.getSession().getAttribute("current-user");
        if (current == null) {
            String act = request.getParameter("act");
            if (act.equals("log")) {
                String username = request.getParameter("log_username");
                String password = request.getParameter("log_password");
                if (!username.equals("") && !password.equals("")) {
                    current = UserService.getUserByName(username);
                    password = UserService.passwordHash(password);
                    if (current != null && UserService.passwordCheck(current, password)) {
                        if (current.isAdmin()) {
                            request.getSession().setAttribute("current-admin", current);
                        }
                        request.getSession().setAttribute("current-user", current);
                        String token = UserTokenService.newToken(username);
                        UserTokenService.addToken(username, token);
                        Cookie cookie = new Cookie("itis-bookshelf", token);
                        cookie.setMaxAge(60 * 60);
                        response.addCookie(cookie);
                        response.sendRedirect("/");
                    } else
                        response.sendRedirect("/login");
                } else {
                    response.sendRedirect("/login");
                }
            } else if (act.equals("reg")) {
                String name = request.getParameter("name");
                String username = request.getParameter("username");
                String password = request.getParameter("password");
                String email = request.getParameter("email");
                String city = request.getParameter("city");
                String year = request.getParameter("year");
                if (!name.equals("") && !username.equals("") && !password.equals("") &&
                        !email.equals("") && !city.equals("") && !year.equals("")) {
                    String token = UserTokenService.newToken(username);
                    password = UserService.passwordHash(password);
                    current = new User(name, username, password, email, city,
                            Integer.valueOf(year), false, token
                    );
                    UserService.addUser(current);
                    request.getSession().setAttribute("current-user", current);
                    Cookie cookie = new Cookie("itis-bookshelf", token);
                    cookie.setMaxAge(60 * 60);
                    response.addCookie(cookie);
                    response.sendRedirect("/");
                } else
                    response.sendRedirect("/login");
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getSession().getAttribute("current-user") != null) {
            response.sendRedirect("/");
        }
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        Freemarker.start("index.ftl", root, response);
    }


}