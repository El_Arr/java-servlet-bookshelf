package itis.bookshelf.servlet.filter;

import itis.bookshelf.entity.User;
import itis.bookshelf.service.entity.UserTokenService;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class CookieFilter implements Filter {

    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
            throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpSession session = request.getSession();
        User currentUser = (User) session.getAttribute("current-user");
        if (currentUser == null) {
            Cookie[] wookies = request.getCookies();
            if (wookies != null) {
                String token = "";
                boolean gotWookie = false;
                for (int i = 0; i < wookies.length && !gotWookie; i++) {
                    Cookie wookie = wookies[i];
                    if ("itis-bookshelf".equals(wookie.getName())) {
                        token = wookie.getValue();
                        gotWookie = true;
                    }
                }
                if (gotWookie) {
                    currentUser = UserTokenService.getUserByToken(token);
                    if (currentUser != null) {
                        if (currentUser.isAdmin()) {
                            session.setAttribute("current-admin", currentUser);
                        }
                        session.setAttribute("current-user", currentUser);
                    }
                }
            }
        }
        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
