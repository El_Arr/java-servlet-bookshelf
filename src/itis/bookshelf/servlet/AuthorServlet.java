package itis.bookshelf.servlet;

import itis.bookshelf.cfg.view.Freemarker;
import itis.bookshelf.entity.Author;
import itis.bookshelf.entity.BookAuthor;
import itis.bookshelf.service.entity.AuthorService;
import itis.bookshelf.service.entity.BookAuthorService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AuthorServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        Map<String, Object> root = new HashMap<>();
        if (request.getSession().getAttribute("current-user") != null)
            root.put("logged", true);
        else
            root.put("logged", false);
        Integer id = Integer.valueOf(request.getParameter("id"));
        if (id != null) {
            Author author = AuthorService.getById(id);
            root.put("author", author);
            List<BookAuthor> baList = BookAuthorService.getAll();
            List<BookAuthor> res = new ArrayList<>();
            if (baList != null) {
                for (BookAuthor ba : baList) {
                    if (ba.getAuthor().getId() == id) {
                        res.add(ba);
                    }
                }
                root.put("books_authors", res);
            }
        }
        Freemarker.start("authors_id=x.ftl", root, response);
    }

}