package itis.bookshelf.cfg.view;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.Map;

public class Freemarker {
    private static Freemarker freemarker = new Freemarker();
    private static Configuration cfg;

    private Freemarker() {
        try {
            cfg = new Configuration(Configuration.VERSION_2_3_20);
            cfg.setDirectoryForTemplateLoading(new File(
                    "C:\\Users\\Admin\\Git\\Projects\\Bookshelf\\web\\templates"
            ));
            cfg.setDefaultEncoding("utf-8");
            cfg.setLocale(Locale.ENGLISH);
            cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Configuration getCfg() {
        return cfg;
    }

    public static void start(String template, Map<String, Object> root, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            cfg.getTemplate(template).
                    process(root, response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}
