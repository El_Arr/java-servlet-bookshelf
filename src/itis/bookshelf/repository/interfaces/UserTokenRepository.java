package itis.bookshelf.repository.interfaces;

import itis.bookshelf.repository.Repository;

public interface UserTokenRepository extends Repository {

    void addToken(String username, String token);

    void updateToken(String user, String token);

    void deleteToken(String userId);

    Integer findIdByToken(String token);

}
