package itis.bookshelf.repository.interfaces;

import itis.bookshelf.entity.BookAuthorGenre;
import itis.bookshelf.repository.Repository;

import java.util.List;

public interface BookAuthorGenreRepository extends Repository<BookAuthorGenre> {

    List<BookAuthorGenre> getByQuery(String query);

}
