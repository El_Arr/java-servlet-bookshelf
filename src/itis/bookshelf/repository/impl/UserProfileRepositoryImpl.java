package itis.bookshelf.repository.impl;

import itis.bookshelf.entity.User;
import itis.bookshelf.repository.interfaces.UserProfileRepository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserProfileRepositoryImpl implements UserProfileRepository {

    public void addUser(User u) {
        try {
            PreparedStatement st1 = conn.prepareStatement(
                    "INSERT INTO " +
                            "user_profile (username, login, password, email, city, year_birth, is_admin, token) " +
                            "VALUES (" +
                            "'" + u.getUsername() + "', " +
                            "'" + u.getLogin() + "', " +
                            "'" + u.getPassword() + "', " +
                            "'" + u.getEmail() + "', " +
                            "'" + u.getCity() + "', " +
                            u.getYear_birth() + ", " +
                            u.isAdmin() + ", " +
                            "'" + u.getToken() + "');"
            );
            st1.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateUser(User u) {
        try {
            PreparedStatement st1 = conn.prepareStatement(
                    "UPDATE user_profile " +
                            "SET username='" + u.getUsername() + "', " +
                            "login='" + u.getLogin() + "', " +
                            " password='" + u.getPassword() + "', " +
                            "email='" + u.getEmail() + "', " +
                            "city='" + u.getCity() + "', " +
                            "year_birth=" + u.getYear_birth() + ", " +
                            "is_admin=" + u.isAdmin() + ", " +
                            "token='" + u.getToken() + "'" +
                            "WHERE id=" + u.getId()
            );
            st1.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteUser(User u) {
        int id = u.getId();
        try {
            PreparedStatement st1 = conn.prepareStatement(
                    "DELETE FROM user_profile WHERE id = " + id + ";"
            );
            st1.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<User> getByQuery(String query) {
        List<User> list = new ArrayList<>();
        try {
            PreparedStatement st = conn.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                list.add(
                        new User(
                                rs.getString("username"),
                                rs.getString("login"),
                                rs.getString("password"),
                                rs.getString("email"),
                                rs.getString("city"),
                                rs.getInt("year_birth"),
                                rs.getBoolean("is_admin"),
                                rs.getString("token"),
                                rs.getInt("id")
                        )
                );
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}