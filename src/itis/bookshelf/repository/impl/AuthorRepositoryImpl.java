package itis.bookshelf.repository.impl;

import itis.bookshelf.entity.Author;
import itis.bookshelf.repository.interfaces.AuthorRepository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AuthorRepositoryImpl implements AuthorRepository {

    public List<Author> getByQuery(String query) {
        List<Author> list = new ArrayList<>();
        try {
            PreparedStatement st = conn.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(
                        new Author(
                                rs.getString("name"),
                                rs.getString("biography"),
                                rs.getString("img_path"),
                                rs.getInt("id")
                        )
                );
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
