package itis.bookshelf.repository.impl;

import itis.bookshelf.entity.Book;
import itis.bookshelf.repository.interfaces.BookRepository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BookRepositoryImpl implements BookRepository {

    public List<Book> getByQuery(String query) {
        List<Book> list = new ArrayList<>();
        try {
            PreparedStatement st = conn.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(
                        new Book(
                                rs.getString("name"),
                                rs.getString("description"),
                                rs.getString("cover_path"),
                                rs.getInt("topic_id"),
                                rs.getInt("year_pub"),
                                rs.getInt("pages"),
                                rs.getString("intro_path"),
                                rs.getString("language"),
                                rs.getBoolean("is_popular"),
                                rs.getBoolean("is_new"),
                                rs.getInt("id")
                        )
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

}
