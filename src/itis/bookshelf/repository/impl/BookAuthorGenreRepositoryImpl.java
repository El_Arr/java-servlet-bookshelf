package itis.bookshelf.repository.impl;

import itis.bookshelf.entity.Author;
import itis.bookshelf.entity.Book;
import itis.bookshelf.entity.BookAuthorGenre;
import itis.bookshelf.entity.Genre;
import itis.bookshelf.repository.interfaces.BookAuthorGenreRepository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BookAuthorGenreRepositoryImpl implements BookAuthorGenreRepository {

    public List<BookAuthorGenre> getByQuery(String query) {
        List<BookAuthorGenre> list = new ArrayList<>();
        try {
            PreparedStatement st = conn.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new BookAuthorGenre(
                        new Book(
                                rs.getString("book_name"),
                                rs.getString("description"),
                                rs.getString("cover_path"),
                                rs.getInt("topic_id"),
                                rs.getInt("year_pub"),
                                rs.getInt("pages"),
                                rs.getString("intro_path"),
                                rs.getString("language"),
                                rs.getBoolean("is_popular"),
                                rs.getBoolean("is_new"),
                                rs.getInt("book_id")
                        ),
                        new Author(
                                rs.getString("author_name"),
                                rs.getString("biography"),
                                rs.getString("img_path"),
                                rs.getInt("author_id")
                        ),
                        new Genre(
                                rs.getString("genre_name"),
                                rs.getInt("genre_id")
                        )
                ));
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
