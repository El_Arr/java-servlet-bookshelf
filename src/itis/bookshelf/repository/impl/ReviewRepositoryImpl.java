package itis.bookshelf.repository.impl;

import itis.bookshelf.entity.Review;
import itis.bookshelf.repository.interfaces.ReviewRepository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ReviewRepositoryImpl implements ReviewRepository {

    public List<Review> getByQuery(String query) {
        List<Review> list = new ArrayList<>();
        try {
            PreparedStatement st = conn.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(
                        new Review(
                                rs.getInt("user_id"),
                                rs.getInt("book_id"),
                                rs.getString("text"),
                                rs.getInt("mark"),
                                rs.getString("title"),
                                rs.getInt("like_quantity"),
                                rs.getString("data_pub"),
                                rs.getString("time_pub"),
                                rs.getInt("id")
                        )
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

}
