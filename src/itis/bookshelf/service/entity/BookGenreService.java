package itis.bookshelf.service.entity;

import itis.bookshelf.entity.BookGenre;
import itis.bookshelf.repository.Repository;
import itis.bookshelf.repository.impl.BookGenreRepositoryImpl;

import java.util.List;

public class BookGenreService {
    private static Repository<BookGenre> bookGenreRepository = new BookGenreRepositoryImpl();

    public static List<BookGenre> getAll() {
        return bookGenreRepository.getByQuery(
                "SELECT\n" +
                        "  book.name  AS book_name,\n" +
                        "  description,\n" +
                        "  cover_path,\n" +
                        "  topic_id,\n" +
                        "  year_pub,\n" +
                        "  pages,\n" +
                        "  intro_path,\n" +
                        "  language,\n" +
                        "  is_popular,\n" +
                        "  is_new,\n" +
                        "  book.id    AS book_id,\n" +
                        "  genre.name AS genre_name,\n" +
                        "  genre.id   AS genre_id\n" +
                        "FROM book_genre\n" +
                        "  INNER JOIN book ON book_genre.book_id = book.id\n" +
                        "  INNER JOIN genre ON book_genre.genre_id = genre.id"
        );
    }

    public static BookGenre getByBookId(Integer id) {
        List<BookGenre> booksGenres = bookGenreRepository.getByQuery(
                "SELECT\n" +
                        "  book.name  AS book_name,\n" +
                        "  description,\n" +
                        "  cover_path,\n" +
                        "  topic_id,\n" +
                        "  year_pub,\n" +
                        "  pages,\n" +
                        "  intro_path,\n" +
                        "  language,\n" +
                        "  is_popular,\n" +
                        "  is_new,\n" +
                        "  book.id    AS book_id,\n" +
                        "  genre.name AS genre_name,\n" +
                        "  genre.id   AS genre_id\n" +
                        "FROM book_genre\n" +
                        "  INNER JOIN book ON book_genre.book_id = book.id\n" +
                        "  INNER JOIN genre ON book_genre.genre_id = genre.id\n" +
                        "WHERE book_id = " + id
        );
        if (booksGenres != null)
            return booksGenres.get(0);
        return null;
    }
}
