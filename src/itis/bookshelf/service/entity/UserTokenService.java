package itis.bookshelf.service.entity;

import itis.bookshelf.entity.User;
import itis.bookshelf.repository.impl.UserProfileRepositoryImpl;
import itis.bookshelf.repository.impl.UserTokenRepositoryImpl;
import itis.bookshelf.repository.interfaces.UserProfileRepository;
import itis.bookshelf.repository.interfaces.UserTokenRepository;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.security.SecureRandom;
import java.util.List;

public class UserTokenService {
    private static UserTokenRepository tokenRepository = new UserTokenRepositoryImpl();
    private static UserProfileRepository profileRepository = new UserProfileRepositoryImpl();

    public static String newToken(String username) {
        long longToken = Math.abs((new SecureRandom()).nextLong());
        String random = Long.toString(longToken, 16);
        return username + ":" + random;
    }

    public static void addToken(String username, String token) {
        tokenRepository.addToken(username, token);
    }

    public static User getUserByToken(String token) {
        Integer userId = tokenRepository.findIdByToken(token);
        if (userId != null) {
            List<User> list = profileRepository.getByQuery(
                    "SELECT * FROM user_profile " +
                            "WHERE id=" + userId + ";"
            );
            if (list != null && !list.isEmpty())
                return list.get(0);
        }
        return null;
    }

    public static void removeToken(String username, HttpServletRequest request, HttpServletResponse response) {
        tokenRepository.deleteToken(username);
        HttpSession session = request.getSession();
        Cookie[] cookies = request.getCookies();
        boolean cookieRemoved = false;
        for (int i = 0; i < cookies.length && !cookieRemoved; i++) {
            Cookie cookie = cookies[i];
            if (cookie.getName().equals("itis-bookshelf")) {
                Cookie n = new Cookie("itis-bookshelf", null);
                n.setMaxAge(0);
                response.addCookie(n);
                cookieRemoved = true;
            }
        }
        session.setAttribute("current-user", null);
        session.setAttribute("current-admin", null);
        try {
            request.logout();
        } catch (ServletException e) {
            e.printStackTrace();
        }

    }

}
