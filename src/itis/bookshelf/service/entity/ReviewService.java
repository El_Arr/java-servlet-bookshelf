package itis.bookshelf.service.entity;

import itis.bookshelf.entity.Review;
import itis.bookshelf.repository.Repository;
import itis.bookshelf.repository.impl.ReviewRepositoryImpl;

import java.util.List;

public class ReviewService {
    private static Repository<Review> reviewRepository = new ReviewRepositoryImpl();

    public static List<Review> getByBookId(Integer id) {
        return reviewRepository.getByQuery(
                "SELECT\n" +
                        "  user_id,\n" +
                        "  book_id,\n" +
                        "  text,\n" +
                        "  mark,\n" +
                        "  title,\n" +
                        "  like_quantity,\n" +
                        "  data_pub,\n" +
                        "  time_pub,\n" +
                        "  review.id AS id\n" +
                        "FROM review\n" +
                        "  JOIN book ON review.book_id = book.id\n" +
                        "WHERE review.book_id = " + id + ";"
        );
    }
}
