package itis.bookshelf.service.entity;

import itis.bookshelf.entity.BookAuthorGenre;
import itis.bookshelf.repository.Repository;
import itis.bookshelf.repository.impl.BookAuthorGenreRepositoryImpl;

import java.util.List;

public class BookAuthorGenreService {
    private static Repository<BookAuthorGenre> bookGenreRepository = new BookAuthorGenreRepositoryImpl();

    public static List<BookAuthorGenre> getAll() {
        return bookGenreRepository.getByQuery(
                "WITH book_genre_t AS (SELECT\n" +
                        "                        book.name  AS book_name,\n" +
                        "                        description,\n" +
                        "                        cover_path,\n" +
                        "                        topic_id,\n" +
                        "                        year_pub,\n" +
                        "                        pages,\n" +
                        "                        intro_path,\n" +
                        "                        LANGUAGE,\n" +
                        "                        is_popular,\n" +
                        "                        is_new,\n" +
                        "                        book.id    AS book_id,\n" +
                        "                        genre.name AS genre_name,\n" +
                        "                        genre.id   AS genre_id\n" +
                        "                      FROM book_genre\n" +
                        "                        INNER JOIN book ON book_genre.book_id = book.id\n" +
                        "                        INNER JOIN genre ON book_genre.genre_id = genre.id)\n" +
                        "SELECT\n" +
                        "  book_name,\n" +
                        "  description,\n" +
                        "  cover_path,\n" +
                        "  topic_id,\n" +
                        "  year_pub,\n" +
                        "  pages,\n" +
                        "  intro_path,\n" +
                        "  LANGUAGE,\n" +
                        "  is_popular,\n" +
                        "  is_new,\n" +
                        "  book_genre_t.book_id,\n" +
                        "  author.name AS author_name,\n" +
                        "  biography,\n" +
                        "  img_path,\n" +
                        "  author.id   AS author_id,\n" +
                        "  genre_name,\n" +
                        "  genre_id\n" +
                        "FROM book_author\n" +
                        "  JOIN book_genre_t ON book_author.book_id = book_genre_t.book_id\n" +
                        "  JOIN author ON book_author.author_id = author.id"
        );
    }

}
