package itis.bookshelf.service.entity;

import itis.bookshelf.entity.User;
import itis.bookshelf.repository.impl.UserProfileRepositoryImpl;
import itis.bookshelf.repository.interfaces.UserProfileRepository;
import itis.bookshelf.service.other.PasswordEncryptor;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserService {
    private static UserProfileRepository userRepository = new UserProfileRepositoryImpl();

    public static void addUser(User u) {
        userRepository.addUser(u);
    }

    public static User getUserByName(String username) {
        List<User> list = userRepository.getByQuery(
                "SELECT * FROM user_profile WHERE username = '" + username + "'"
        );
        if (list != null && !list.isEmpty()) {
            User u = list.get(0);
            if (u != null)
                return u;
        }
        return null;
    }

    public static String passwordHash(String password) {
        return PasswordEncryptor.cryptWithMD5(password);
    }

    public static boolean passwordCheck(User user, String password) {
        Pattern p = Pattern.compile("([A-Z]|[a-z]|[0-9]|\\.)*");
        Matcher matcher = p.matcher(password);
        return password.equals(user.getPassword()) && matcher.matches();
    }

}
