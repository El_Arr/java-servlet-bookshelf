package itis.bookshelf.service.other;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class UpdateQuery {
    private static java.sql.Connection conn = itis.bookshelf.cfg.database.Connection.getConnection();

    public static void execute(String query) {
        try {
            PreparedStatement st = conn.prepareStatement(query);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
