package itis.bookshelf.entity;

public class Book {
    private String name;
    private String description;
    private String cover_path;
    private int topic_id;
    private int year_pub;
    private int pages;
    private String intro_path;
    private String language;
    private boolean is_popular;
    private boolean is_new;
    private int id;

    public Book(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public Book(String name, String description, String cover_path, int topic_id,
                int year_pub, int pages, String intro_path, String language,
                boolean is_popular, boolean is_new, int id) {
        this.name = name;
        this.description = description;
        this.cover_path = cover_path;
        this.topic_id = topic_id;
        this.year_pub = year_pub;
        this.pages = pages;
        this.intro_path = intro_path;
        this.language = language;
        this.is_popular = is_popular;
        this.is_new = is_new;
        this.id = id;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getCover_path() {
        return cover_path;
    }

    public int getTopic_id() {
        return topic_id;
    }

    public int getYear_pub() {
        return year_pub;
    }

    public int getPages() {
        return pages;
    }

    public String getIntro_path() {
        return intro_path;
    }

    public String getLanguage() {
        return language;
    }

    public int getId() {
        return id;
    }

    public boolean isIs_popular() {
        return is_popular;
    }

    public boolean isIs_new() {
        return is_new;
    }
}
