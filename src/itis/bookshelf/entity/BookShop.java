package itis.bookshelf.entity;

public class BookShop {
    private String name;
    private String description;
    private String cover_path;
    private int slider_id;
    private int topic_id;
    private int year_pub;
    private int pages;
    private String intro_path;
    private String language;
    private boolean is_popular;
    private boolean is_new;
    private int id;

}
