package itis.bookshelf.entity;

public class Review {
    private int user_id;
    private int book_id;
    private String text;
    private int mark;
    private String title;
    private int like_quantity;
    private String data_pub;
    private String time_pub;
    private int id;

    public Review(int user_id, int book_id, String text, int mark, String title, int like_quantity, String data_pub, String time_pub, int id) {
        this.user_id = user_id;
        this.book_id = book_id;
        this.text = text;
        this.mark = mark;
        this.title = title;
        this.like_quantity = like_quantity;
        this.data_pub = data_pub;
        this.time_pub = time_pub;
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public int getBook_id() {
        return book_id;
    }

    public String getText() {
        return text;
    }

    public int getMark() {
        return mark;
    }

    public String getTitle() {
        return title;
    }

    public int getLike_quantity() {
        return like_quantity;
    }

    public String getData_pub() {
        return data_pub;
    }

    public String getTime_pub() {
        return time_pub;
    }

    public int getId() {
        return id;
    }

}
