package itis.bookshelf.entity;

public class BookAuthorGenre {
    private Book book;
    private Author author;
    private Genre genre;

    public BookAuthorGenre(Book book, Author author, Genre genre) {
        this.book = book;
        this.author = author;
        this.genre = genre;
    }

    public Book getBook() {
        return book;
    }

    public Author getAuthor() {
        return author;
    }

    public Genre getGenre() {
        return genre;
    }
}
